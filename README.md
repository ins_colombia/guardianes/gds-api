# 0. Passo Preliminar

* Atualização do _Package Manager_

```shell
sudo apt-get update
sudo apt-get -y upgrade
```

# 1. NodeJs

* Instalação de pacotes básicos

```shell
sudo apt-get install -y build-essential
sudo apt-get install -y curl
```

* Instalação do NodeJs 8.x

```shell
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

# 2. Projeto API

* Importação do projeto

```shell
sudo apt-get install -y git-core
mkdir /opt/gds
cd /opt/gds
git clone -b spanish https://gitlab.com/centeias/gds-api.git api
```

* Instalação do DBMS MongoDB

```shell
sudo apt-get install -y mongodb
sudo apt-get install -y libkrb5-dev
```

* Instalação das dependências do projeto

```shell
sudo npm install sails -g
cd api
npm install kerberos
npm install grunt
npm install
```

* Inicialização do serviço

```shell
sudo npm install pm2 -g
bin/start.sh
```

# 3. Ruby

* Instalação de pacotes básicos

```shell
sudo apt-get install -y autoconf
sudo apt-get install -y bison
sudo apt-get install -y libssl-dev
sudo apt-get install -y libyaml-dev
sudo apt-get install -y libreadline6-dev
sudo apt-get install -y zlib1g-dev
sudo apt-get install -y libncurses5-dev
sudo apt-get install -y libffi-dev
sudo apt-get install -y libgdbm3
sudo apt-get install -y libgdbm-dev
```

* Instalação do rbenv

```shell
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
source ~/.bashrc
```

* Instalação ruby-build

```shell
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
```

* Instalação ruby 2.4.1

```shell
rbenv install 2.4.1
rbenv global 2.4.1
```

* Instalação bundler

```shell
echo "gem: --no-document" > ~/.gemrc
gem install bundler
```

# 4. Projeto Dashboard

* Importação do projeto

```shell
cd /opt/gds
git clone -b spanish https://gitlab.com/centeias/gds-dashboard.git dashboard
```

* Instalação das dependências do projeto

```shell
cd dashboard
bundler install
```

* Inicialização do serviço

```shell
bin/start.sh
```

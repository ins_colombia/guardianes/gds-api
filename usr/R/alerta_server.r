## Alerta versão para o server

library(rmongodb)  #R para mongo
library(dplyr)     #gerenciamento dados
library(msir)      #loess
library(rjson)    #json

setwd('~/api/usr/R')
source('funcoesV3.r')   ## funcoes auxiliares

## pega o CSV
exporta <- read.csv2("https://s3.amazonaws.com/gdsreports/surveys_reports.csv",as.is=TRUE)

exporta$DT_CADASTRO <- as.Date(exporta$DT_CADASTRO)
exporta$DT_REGISTRO_DIA <- as.Date(exporta$DT_REGISTRO_DIA)
exporta$CADASTRO <- as.Date(exporta$CADASTRO)

filtro <- exporta$DT_REGISTRO_DIA >= as.Date('2016-07-12')


## remove inicio com fase de testes
guardioes <- exporta[filtro,]

## cria sindromes 
sindrome <- rep('NAO',nrow(guardioes))
sindrome <- ifelse(guardioes$DIARREIA=='SIM','DIARREICA',sindrome)
sindrome <- ifelse(guardioes$SIND_RES=='SIM','RESPIRATORIA',sindrome)
guardioes$SINDROME <- ifelse(guardioes$SIND_EXA=='SIM','EXANTEMATICA',sindrome)


## Cidades 
cidades <- c("Rio de Janeiro","São Paulo","Brasília","Salvador","Belo Horizonte","Manaus","Brasil")

sindromes <- c('DIARREICA','RESPIRATORIA','EXANTEMATICA')


resp <- NULL

count <- 1 ## contador

result <- list()  ## lista para resultados

for (loc in cidades) {
    # tmp <- geraAlertaRegioes(lista_sintomas,reg=cidades[i])
    # resp <- rbind(resp,tmp)
    for (sind in sindromes) {
        result[[count]] <- geraAlerta (guardioes,loc,sind,alisa=0.8,sigma=1,corte=3,debug=FALSE)
         
        count <- count+1
    }
}

resp <- do.call(rbind,result)  ## transforma em DF

lixo <- as.Date(resp$data)
l2 <- max(lixo) - 7           ## exposta somente os ultimos 7 dias 

df <- resp[lixo >= l2,]

df_list <- lapply(split(df, 1:nrow(df)), function(x) mongo.bson.from.JSON(toJSON(x)))



# 
 #system("ssh -L 12345:localhost:27017 ubuntu@107.170.6.73 -N &")  ## cria o canal SSH! quando for rodar no local não usar!!!
# 

mongo <- mongo.create(host='127.0.0.1:27017') #no localhost mudar  a porta para a  default

 
if(mongo.is.connected(mongo) != TRUE) stop ("sem conexão com o Mongo")

mongo.remove(mongo,'epihack.alertas')
mongo.insert.batch(mongo, 'epihack.alertas', df_list) ## escreve no banco

mongo.destroy(mongo)  ## fecha a conexão





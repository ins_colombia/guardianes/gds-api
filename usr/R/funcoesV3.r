## Funcoes  Guardioes nova


geraAlerta <- function(guard,local='Rio de Janeiro',sindrome,alisa=0.8,sigma=1,corte=3,debug=FALSE) {
    
    require(dplyr)
    require(forecast)
    require(msir) 
    
    guard$data <- guard$DT_REGISTRO_DIA
    f1 <- guard$CIDADE==local 
    f2 <- guard$SINDROME==sindrome
    
    ## filtro Brasil
    if(local == 'Brasil') f1 <- guard$FORAPAIS=='NAO'
    
    
    sinal <- filter(guard,f1 & f2) %>%  select(data) %>% group_by(data) %>% summarize(casos= n())
   
     if (nrow(sinal) < 5) return(structure(list(data = as.character(Sys.Date()), casos = 0.0, regiao = local, 
                                                sindrome = sindrome,ponto_corte = 0.0, 
                                                classifica = 0, fcod = "VERDE", fcor = "#68ba44", msg = "Normal", 
                                                alisa = 0.3, sigma = sigma, corte = corte), 
                                               .Names = c("data", "casos", "regiao", "sinal", "ponto_corte",
                                                          "classifica", "fcod", "fcor", "msg", "alisa", "sigma",
                                                          "corte"), row.names = 54L, class = "data.frame"))
                                            
    
    #if(regiao != 'BRASIL')  
    #serie <- xts(sinal$casos,order.by = as.Date(sinal$data))
    
    rg <- range(sinal$data)
    dd <- seq(rg[1],rg[2],by='1 day')
    sinal <- merge(sinal,data.frame(dd),by.x='data',by.y='dd',all.y=T)
    sinal$casos <- ifelse(is.na(sinal$casos),0.01,sinal$casos)


    
    if (debug) print (paste("Numero de Registros =",nrow(sinal)) ) 
    
    ## adicionado o predito por AR(1) como ultimo registro
    sinal <- data.frame (
        data=c(sinal$data,as.Date(max(sinal$data)+1,origin='1970-01-01')) ,
        casos=c(sinal$casos,predict(ar(sinal$casos,1),n.ahead = 1)$pred)
        
    )
    
    
    ## cria o loess com intervalo de confianca
    tres <- loess.sd(sinal$casos,span=alisa,nsigma = sigma)
    
   if(debug) {
   plot(sinal$data,sinal$casos,type='l')
   lines(sinal$data,tres$y,col=1,lwd=1,lty=2)
   lines(sinal$data,tres$upper,col=2,lwd=2)
   title(paste0(local, '\t Sindrome  ',sindrome))
   }
    
   
    # Amarelo testa se esta acima e sem foram mais de X casos
    altos <- ifelse(sinal$casos > tres$upper & sinal$casos > corte,TRUE,FALSE)
    
    
    # o segundo amarelo seguido e convertido em laranja
    
    laranja <- rep(FALSE,length(altos))
    
    for(i in 2:(length(altos)-1))  
        if((altos[i] & altos[i-1]) & sinal$casos[i] > corte) laranja[i] <- TRUE 
    
    
    # o segundo laranja seguido vira vermelho
    vermelho <- rep(FALSE,length(altos))
    for(i in 2:(length(laranja)-1))  
        if((laranja[i] & laranja[i-1]) & sinal$casos[i] > corte) vermelho[i] <- TRUE 
    
    ### estruturando o output
    sinal$regiao <- local
    
    sinal$sinal <- sindrome
    sinal$ponto_corte <- tres$upper
    sinal$classifica <- altos+laranja+vermelho
    
    sinal$classifica <- ifelse(sinal$casos < corte, 0 ,sinal$classifica)
    
    sinal$fcod <- ifelse(sinal$classifica==0,"VERDE",NA)
    sinal$fcod <- ifelse(sinal$classifica==1,"AMARELO",sinal$fcod)
    sinal$fcod <- ifelse(sinal$classifica==2,"LARANJA",sinal$fcod)
    sinal$fcod <- ifelse(sinal$classifica==3,"VERMELHO",sinal$fcod)
    
    sinal$fcor <- ifelse(sinal$classifica==0,"#68ba44",NA)
    sinal$fcor <- ifelse(sinal$classifica==1,"#e8a512",sinal$fcor)
    sinal$fcor <- ifelse(sinal$classifica==2,"#f47821",sinal$fcor)
    sinal$fcor <- ifelse(sinal$classifica==3,"#d0021b",sinal$fcor)
    
    
    
    
    
    sinal$msg  <- ifelse(sinal$classifica==1,paste("Aumento de ",sindrome) , "Normal")
    sinal$msg  <- ifelse(sinal$classifica==2,paste("Aumento sustentado de ",sindrome) , sinal$msg)
    sinal$msg  <- ifelse(sinal$classifica==3,paste("Pico de ",sindrome) , sinal$msg)
    
    
    #retorna a os dados
    
    sinal$alisa <- alisa
    sinal$sigma <- sigma
    sinal$corte <- corte
    
    sinal$data <- as.character(sinal$data)
    
    tam <- nrow(sinal)
    #sinal[(tam-7):tam,]
    
    tail(sinal,7)
    
}



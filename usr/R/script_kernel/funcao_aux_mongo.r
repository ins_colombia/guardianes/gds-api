lemongo <- function(canal,tabela,debug=FALSE) {
    require(plyr)
## create the empty data frame
    data <-  data.frame(stringsAsFactors = FALSE)

## create the namespace


## create the cursor we will iterate over, basically a select * in SQL
cursor <-  mongo.find(canal, tabela)

## create the counter
i = 1

## iterate over the cursor
while (mongo.cursor.next(cursor)) {
    # iterate and grab the next record
    tmp <- mongo.bson.to.list(mongo.cursor.value(cursor))
    # make it a dataframe
    tmp.df <- as.data.frame(t(unlist(tmp)), stringsAsFactors = F)
    # bind to the master dataframe
    data <- rbind.fill(data, tmp.df)
    # to print a message, uncomment the next 2 lines 
    if (debug) {
         cat('read line #', i,  '\n')
        i = i +1
    }
}

data
}
